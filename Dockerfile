FROM maven:3.5-jdk-8
WORKDIR /app
COPY . .
RUN mvn package
EXPOSE 5555
ENTRYPOINT ["java","-jar","target/micro_gateway-1.0.jar"]